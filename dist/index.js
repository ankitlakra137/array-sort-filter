"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mySort = exports.myFilter = void 0;
var array_utilities_1 = require("./array-utilities");
Object.defineProperty(exports, "myFilter", { enumerable: true, get: function () { return array_utilities_1.myFilter; } });
Object.defineProperty(exports, "mySort", { enumerable: true, get: function () { return array_utilities_1.mySort; } });
