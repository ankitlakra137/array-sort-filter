"use strict";
// Filter Array
Object.defineProperty(exports, "__esModule", { value: true });
exports.mySort = exports.myFilter = void 0;
function myFilter(arr, filterFunc) {
    const tempArr = [];
    for (let i = 0; i < arr.length; i++) {
        if (filterFunc(arr[i])) {
            tempArr.push(arr[i]);
        }
    }
    return tempArr;
}
exports.myFilter = myFilter;
function mySort(arr, callBackFn) {
    return (arr.sort(callBackFn));
}
exports.mySort = mySort;
