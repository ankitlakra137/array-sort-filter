// Filter Array

interface filterCallBackFunc {
  (el:any, i?:number, arr?:any[]): boolean;
}

export function myFilter(arr: any[], filterFunc: filterCallBackFunc){

  const tempArr: any[] = [];
  for(let i:number = 0; i<arr.length; i++){
    if(filterFunc(arr[i])){
      tempArr.push(arr[i]);
    }
  }

  return tempArr;
}

// Sort Array

interface sortCallBackFunc {
  (a:any, b:any): number;
}

export function mySort(arr: any[], callBackFn: sortCallBackFunc) {
  return (arr.sort(callBackFn));
}