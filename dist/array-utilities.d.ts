interface filterCallBackFunc {
    (el: any, i?: number, arr?: any[]): boolean;
}
export declare function myFilter(arr: any[], filterFunc: filterCallBackFunc): any[];
interface sortCallBackFunc {
    (a: any, b: any): number;
}
export declare function mySort(arr: any[], callBackFn: sortCallBackFunc): any[];
export {};
